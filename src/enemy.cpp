#include "enemy.h"
#include "condition.h"

Enemy::Enemy(){}

Enemy::Enemy(std::string name, int max_hp, double atk_low, double atk_high){
	name_     = name;
	hp_       = max_hp;
	max_hp_   = max_hp;
	atk_low_  = atk_low;
	atk_high_ = atk_high;
}

Enemy::~Enemy(){
	for( unsigned int x = 0; x < conditions_.size(); ++x ){
		conditions_[x]->Clear();
		conditions_.erase(conditions_.begin()+x);
	}
}
