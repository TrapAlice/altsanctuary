#include "world.h"
#include "game.h"
#include "enemy.h"
#include "character.h"
#include "renderer.h"
#include "gamestate.h"

#include "statemainmenu.h"
#include "statecharacterselect.h"
#include "stategamemode.h"
#include "stateinventory.h"
#include "statecombat.h"
#include "stateskillswap.h"

World::World(Game* g){
	game_ = g;
}

World::~World(){
	if( player_ )        delete player_;
	if( current_enemy_ ) delete current_enemy_;
}

void World::Update(char c){
	current_state_->Update(this, c);
}

void World::Render(Renderer *r){
	current_state_->Render(this, r);
}

Character* World::Player(){ return( player_ ); }

void World::SetPlayer(Character *c){
	player_ = c;
}

Enemy* World::CurrentEnemy(){ return( current_enemy_); }

void World::SetNewEnemy(Enemy *e){
	current_enemy_ = e;
}

void World::End(){
	game_->End();
}

void World::ChangeState(int state){
	if( !current_state_ ) delete(current_state_);
	switch( state ){
		case STATE_MAINMENU:
			current_state_ = new State_MainMenu;
			break;
		case STATE_CHARACTERSELECT:
			current_state_ = new State_CharacterSelect;
			break;
		case STATE_GAMEMODE:
			current_state_ = new State_GameMode;
			break;
		case STATE_INVENTORY:
			current_state_ = new State_Inventory;
			break;
		case STATE_COMBAT:
			current_state_ = new State_Combat;
			break;
		case STATE_SKILLSWAP:
			current_state_ = new State_SkillSwap;
			break;
	}
	current_state_->Init();
}