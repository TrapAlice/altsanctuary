#ifndef _STATE_GAMEMODE_H
#define _STATE_GAMEMODE_H

#include "gamestate.h"

class State_GameMode : public iGameState{
public:
	void Init();
	void Render( World *w, Renderer *r );
	void Update( World *w, char c );
};

#endif