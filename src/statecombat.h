#ifndef _STATE_COMBAT
#define _STATE_COMBAT

#include "gamestate.h"

class Skill;

class State_Combat : public iGameState {
public:
	void Init();
	void Render( World *w, Renderer *r );
	void Update( World *w, char c );
private:
	void _PlayerAttack( World *w, Skill *skill );
	int skill_offset_;
	int player_position_;
	bool repositioning_;
};

#endif
