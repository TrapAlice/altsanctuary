#ifndef _WORLD_H
#define _WORLD_H

class Character;
class iGameState;
class Enemy;
class Renderer;
class Game;

class World{
public:
	World(Game *g);
	~World();
  Character* Player();
  void       SetPlayer( Character *c );
  Enemy*     CurrentEnemy();
  void       SetNewEnemy( Enemy *e );
  void       Render( Renderer *r );
  void       Update( char c );
	void       ChangeState( int state );
	void       End();
private:
	Character  *player_;
	Enemy      *current_enemy_;
	iGameState *current_state_;
	Game       *game_;
};

#endif