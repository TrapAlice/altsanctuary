#include "stategamemode.h"
#include "renderer.h"
#include "character.h"
#include "skill.h"
#include "enemy.h"
#include "world.h"

void State_GameMode::Init(){}

void State_GameMode::Render(World *w, Renderer *r){
	Character *c = w->Player();
	r->prints(0, 0,  "+-----------------------------------------------------------------------------+");
	r->prints(1, 2,  "%s the Lvl %d %s", c->Name().c_str(), 1, c->Class().c_str());
	r->prints(1, 3,  "WEAP: Name [iLvl 0] Effect");
	r->prints(1, 4,  "ARMR: [0%]");
	r->prints(1, 5,  "ITEM: Name [+0]");
	r->prints(1, 7,  "VIT: [%0.1f]", c->Vit());
	r->prints(1, 8,  "STR: [%0.1f]", c->Str());
	r->prints(1, 9,  "INT: [%0.1f]", c->Int());
	r->prints(1, 10, "DEX: [%0.1f]", c->Dex());
	r->prints(0, 11, "+-----------------------------------------------------------------------------+");
	r->prints(0, 13, "> HP: %d/%d", c->Hp(), c->MaxHp());
	r->prints(0, 15, "+-----------------------------------------------------------------------------+");
	r->prints(1, 16, "[1] Test Fight");
	r->prints(1, 17, "[s] Change Skills");
	r->prints(1, 18, "[e] Equipment");
	r->prints(0, 22, "+-----------------------------------------------------------------------------+");

}

void State_GameMode::Update(World *w, char c){
	switch( c ){
		case 'E':
		case 'e':
			w->ChangeState(STATE_INVENTORY);
			break;
		case 'S':
		case 's':
			w->ChangeState(STATE_SKILLSWAP);
			break;
		case '1':
			w->SetNewEnemy(new Enemy("Test", 150, 1, 5));
			w->ChangeState(STATE_COMBAT);
			break;
		case '4':
			w->End();
			break;
	}
}
