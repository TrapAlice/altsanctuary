#include "input.h"
#include <iostream>
#include "libtcod.hpp"

int Input::Key(){
	TCOD_key_t key;
	TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS,&key,NULL);
	return key.c;
}