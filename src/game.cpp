#include "game.h"
#include "world.h"
#include <iostream>
#include "gamestate.h"
#include "input.h"
#include "libtcod.hpp"

#include "renderer.h"

Game::Game(){
	renderer_ = new Renderer();
	world_    = new World(this);
	input_    = new Input();
	done_     = false;
}

Game::~Game(){
	if( renderer_ ) delete renderer_;
	if( world_ )    delete world_;
	if( input_ )    delete input_;
}

void Game::Start(){
	char selection;
	world_->ChangeState(STATE_MAINMENU);
	while( !done_ ){
		world_->Render(renderer_);
		renderer_->Flush();
		selection = input_->Key();
		if( selection == '#' || TCODConsole::isWindowClosed()) done_ = true;
		world_->Update(selection);
	}
}

void Game::End(){
	done_ = true;
}