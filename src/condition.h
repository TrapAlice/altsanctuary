#ifndef _CONDITION_H
#define _CONDITION_H

class Entity;

class Condition{
public:
	static Condition* CreateCondition(int condition_Id);
	Condition(int ticks, bool permanent, void (*on_tick)(Entity*, Entity*), void (*on_attack)(double*, Entity*, Entity*), void (*on_damaged)(double*, Entity*, Entity*));
	void ActivateAttacks(double *damage, Entity*, Entity*);
	void ActivateDamaged(double *damage, Entity*, Entity*);
	void ActivateTick(Entity*, Entity*);
	void Clear();
	bool isActive();
	bool isPermanent();
private:
	int    ticks_remaining_;
	bool   permanent_;
	void (*on_tick_)(Entity*, Entity*);
	void (*on_attack_)(double*, Entity*, Entity*);
	void (*on_damaged_)(double*, Entity*, Entity*);
	bool   active_;
	int    x_;
};

#endif