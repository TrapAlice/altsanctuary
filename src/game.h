#ifndef _GAME_H
#define _GAME_H

class Renderer;
class World;
class Input;

class Game{
public:
	Game();
	~Game();
	void Start();
	void ChangeState( int state );
	void End();
private:
	Renderer  *renderer_;
	Input     *input_;
	World     *world_;
	bool       done_;
};

#endif
