#include "inventory.h"
#include "renderer.h"

Inventory::Inventory(){
	items_[2][3] = "Sword";
}

void Inventory::Render(Renderer *r){
	r->printc(10, 1, '1');
	r->printc(20, 1, '2');
	r->printc(30, 1, '3');
	r->printc(40, 1, '4');
	r->printc(0,  3, 'Q');
	r->printc(0,  6, 'W');
	r->printc(0,  9, 'E');
	r->printc(0, 12, 'R');
	
	for( int y=0; y < 4; y++ ){
		for( int x=0; x < 4; x++ ){
			r->prints(x*10 + 10, y*3 + 3, items_[x][y]);	
		}
	}
}

int Inventory::GetItemId(char *input){
	int x, y, a, b;
	bool flag=0;

	a = input[0]-'1';
	
	if( a > 3 ){
		flag=1;
		switch( a ){
			case 64: //'q'
			case 32:
				a = 0;
				break;
			case 70: //'w'
			case 38:
				a = 1;
				break;
			case 52: //'e'
			case 20:
				a = 2;
				break;
			case 65: //'r'
			case 33:
				a = 3;
				break;
			case 48: //'a'
				return( -1 );
			default:
				return( -2 );
		}
	}
	b= input[1]-'1';
	if( !flag ){
		switch( b ){
			case 64: //'q'
			case 32:
				b = 0;
				break;
			case 70: //'w'
			case 38:
				b = 1;
				break;
			case 52: //'e'
			case 20:
				b = 2;
				break;
			case 65: //'r'
			case 33:
				b = 3;
				break;
			default:
				return( -2 );
		}
	}
	
	if( flag ){
		x=b;
		y=a;
	} else {
		x=a;
		y=b;
	}

	if( x < 0 || x > 3 ||
	    y < 0 || y > 3 ){
		return( -2 );
	}

	return( x+(y*4) );	
}

std::string Inventory::GetItem(int pos){
	std::string item = items_[pos%4][pos/4];
	items_[pos%4][pos/4].clear();
	return item;
} 
