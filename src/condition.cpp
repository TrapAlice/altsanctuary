#include "condition.h"
#include "entity.h"
#include "conditionid.h"
#include "condition_function.cc"

Condition::Condition(int ticks, bool permanent, void (*on_tick)(Entity*, Entity*), void (*on_attack)(double*, Entity*, Entity*), void (*on_damaged)(double*, Entity*, Entity*)){
	ticks_remaining_ = ticks;
	permanent_       = permanent;
	on_tick_         = on_tick;
	on_attack_       = on_attack;
	on_damaged_      = on_damaged;
	active_          = true;
}

void Condition::ActivateAttacks(double *damage, Entity* attacker, Entity* defender){
	if( on_attack_ ) (on_attack_)(damage, attacker, defender);
}

void Condition::ActivateDamaged(double *damage, Entity* attacker, Entity* defender){
	if( on_damaged_ ) (on_damaged_)(damage, attacker, defender);
}

void Condition::ActivateTick(Entity *attacker, Entity *defender){
	if( on_tick_ ) (on_tick_)(attacker, defender);
	ticks_remaining_--;
	if( ticks_remaining_ == 0 ){
		active_ = false;
	}
}

void Condition::Clear(){
	if( !permanent_ ){
		delete this;
	}
}

bool Condition::isActive(){
	return active_;
}

bool Condition::isPermanent(){
	return permanent_;
}

#define CONDITION(id, ticks, permanent, on_tick, on_attack, on_damaged) \
case id: return( new Condition(ticks, permanent, on_tick, on_attack, on_damaged) )

Condition* Condition::CreateCondition(int condition_id){
	switch( condition_id ){
		CONDITION(Condition_Ranger_Poison,        5, false, &ranger_poison,    
			                                                  NULL, 
			                                                  NULL);
		CONDITION(Condition_Ranger_HealTier1,     1, false, &ranger_healtier1, 
			                                                  NULL, 
			                                                  NULL);
		CONDITION(Condition_Ranger_HealTier2,     2, false, &ranger_healtier2,  
			                                                  NULL, 
			                                                  NULL);
		CONDITION(Condition_Ranger_HealTier3,     3, false, &ranger_healtier3,  
			                                                  NULL, 
			                                                  NULL);
		CONDITION(Condition_Ranger_HealTier4,     4, false, &ranger_healtier4,  
			                                                  NULL, 
			                                                  NULL);
		CONDITION(Condition_Ranger_CripplingShot, 2, false, NULL, 
			                                                  &ranger_cripplingshot, 
			                                                  NULL);
		CONDITION(Condition_Ranger_Preparation,   2, false, NULL, 
			                                                  &ranger_preparation, 
			                                                  NULL);
	}
	return NULL;
}
