#include "entity.h"

void ranger_poison(Entity* attacker, Entity* defender){
	defender->TakeDamage(20); 
}

void ranger_healtier1(Entity* attacker, Entity* defender){
	defender->TakeDamage(-100);
}

void ranger_healtier2(Entity* attacker, Entity* defender){
	defender->TakeDamage(-200);
}

void ranger_healtier3(Entity* attacker, Entity* defender){
	defender->TakeDamage(-300);
}

void ranger_healtier4(Entity* attacker, Entity* defender){
	defender->TakeDamage(-400);
}

void ranger_cripplingshot(double *damage, Entity* attacker, Entity* defender){
	*damage /= 2;
}

void ranger_preparation(double *damage, Entity* attacker, Entity* defender){
	*damage *= 1.5;
}
