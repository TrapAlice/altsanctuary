#ifndef _GAMESTATE_H
#define _GAMESTATE_H

class Renderer;
class World;

enum e_states{
	STATE_MAINMENU,
	STATE_CHARACTERSELECT,
	STATE_GAMEMODE,
	STATE_INVENTORY,
	STATE_COMBAT,
	STATE_SKILLSWAP,
};

class iGameState{
public:
	virtual ~iGameState(){};
	virtual void Init() = 0;
	virtual void Render( World *w, Renderer *r ) = 0;
	virtual void Update( World *w, char c ) = 0;
};

#endif
