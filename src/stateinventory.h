#ifndef _STATE_INVENTORY_H
#define _STATE_INVENTORY_H

#include "gamestate.h"

class State_Inventory : public iGameState{
public:
	void Init();
	void Render( World *w, Renderer *r );
	void Update( World *w, char c );
};

#endif
