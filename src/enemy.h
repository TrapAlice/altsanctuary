#ifndef _ENEMY_H
#define _ENEMY_H

#include "entity.h"

class Enemy : public Entity{
public:
	Enemy();
	Enemy(std::string name, int max_hp, double atk_low, double atk_high);
	~Enemy();
	double      AtkLow();
	double      AtkHigh();
protected:
	double       atk_low_;
	double       atk_high_;
	bool         guards_;
};

#endif
